package com.stilbon.template.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Utils {
    private static DateTimeFormatter sDateFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    private static DateTimeFormatter sTimeFormatter = DateTimeFormat.forPattern("HH:mm");

    public static String getFormattedDate(DateTime dt) {
        return sDateFormatter.print(dt);
    }

    public static String getFormattedTime(DateTime dt) {
        return sTimeFormatter.print(dt);
    }
}
