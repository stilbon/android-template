package com.stilbon.template.presenters;

import org.joda.time.DateTime;

public interface SelectDateTimePresenter {
    void sendDateTime(DateTime dt);

    public static interface View {}
}
