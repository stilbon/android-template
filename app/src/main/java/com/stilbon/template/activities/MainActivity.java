package com.stilbon.template.activities;

import com.stilbon.template.fragments.BaseFragment;
import com.stilbon.template.fragments.SelectDateTimeFragment;
import com.stilbon.template.R;

import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_fragment)
public class MainActivity extends BaseActivity {

    @Override
    protected BaseFragment getInitFragment() {
        return SelectDateTimeFragment.newInstance();
    }
}
