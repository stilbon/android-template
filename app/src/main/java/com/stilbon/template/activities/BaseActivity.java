package com.stilbon.template.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.stilbon.template.fragments.BaseFragment;
import com.stilbon.template.R;

public abstract class BaseActivity extends ActionBarActivity {
    private FragmentManager mFm = getSupportFragmentManager();

    public enum AnimationDirection {
        FROM_LEFT_TO_RIGHT,
        FROM_RIGHT_TO_LEFT,
        FROM_TOP_TO_BOTTOM,
        FROM_BOTTOM_TO_TOP
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeFragment(getInitFragment(), false, false, null);
    }

    protected abstract BaseFragment getInitFragment();

    @Override
    public void onBackPressed() {
        mFm.popBackStack();
        if (mFm.getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
    }

    public void changeFragment(Fragment f, boolean addToBackStack, boolean animate, AnimationDirection direction) {
        FragmentTransaction ft = mFm.beginTransaction();

        // Animations
        if (animate) {
            switch (direction) {
                case FROM_RIGHT_TO_LEFT:
                    ft.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_left,
                            R.anim.in_from_right, R.anim.out_to_right);
                    break;
                case FROM_LEFT_TO_RIGHT:
                    ft.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_right,
                            R.anim.in_from_left, R.anim.out_to_left);
                    break;
                case FROM_BOTTOM_TO_TOP:
                    ft.setCustomAnimations(R.anim.in_from_down, R.anim.out_to_down,
                            R.anim.in_from_up, R.anim.out_to_up);
                    break;
                case FROM_TOP_TO_BOTTOM:
                    ft.setCustomAnimations(R.anim.in_from_up, R.anim.out_to_up,
                            R.anim.in_from_down, R.anim.out_to_down);
                    break;
            }
        }

        // Backstack
        if (addToBackStack) {
            ft.addToBackStack(null);
        }

        // Adding fragment
        Fragment oldFragment = mFm.findFragmentById(R.id.fragmentContainer);
        if (oldFragment != null) {
            ft.remove(oldFragment);
        }
        ft.add(R.id.fragmentContainer, f);

        // Commit transaction
        ft.commit();
    }
}
