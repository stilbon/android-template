package com.stilbon.template.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.stilbon.template.MainFragment_;
import com.stilbon.template.R;
import com.stilbon.template.presenters.SelectDateTimePresenter;
import com.stilbon.template.presenters.impls.SelectDateTimePresenterImpl;
import com.stilbon.template.utils.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.joda.time.DateTime;

@EFragment(R.layout.fragment_select_date_time)
public class SelectDateTimeFragment extends BaseFragment
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, SelectDateTimePresenter.View {
    private DateTime mDateTime = DateTime.now();

    @ViewById TextView tvDate;
    @ViewById TextView tvTime;

    private SelectDateTimePresenter mPresenter = new SelectDateTimePresenterImpl(this);

    public static MainFragment_ newInstance() {
        MainFragment_ f = new MainFragment_();
        return f;
    }

    @AfterViews
    protected void afterViews() {
        updateViews();
    }

    private void updateViews() {
        tvDate.setText(Utils.getFormattedDate(mDateTime));
        tvTime.setText(Utils.getFormattedTime(mDateTime));
    }

    @Click(R.id.btnSelectDate)
    protected void clickSelectDate() {
        new DatePickerDialog(mActivity, this,
                mDateTime.getYear(), mDateTime.getMonthOfYear(), mDateTime.getDayOfMonth()).show();
    }

    @Click(R.id.btnSelectTime)
    protected void clickSelectTime() {
        new TimePickerDialog(mActivity, this,
                mDateTime.getHourOfDay(), mDateTime.getMinuteOfHour(), true).show();
    }

    @Click(R.id.btnSend)
    protected void clickSend() {
        mPresenter.sendDateTime(mDateTime);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear++; // 'cause Picker returns Jan as 0th month
        mDateTime = mDateTime.withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth);
        updateViews();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mDateTime = mDateTime.withHourOfDay(hourOfDay).withMinuteOfHour(minute);
        updateViews();
    }
}
