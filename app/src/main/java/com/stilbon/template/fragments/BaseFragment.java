package com.stilbon.template.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.stilbon.template.activities.BaseActivity;

public abstract class BaseFragment extends Fragment {
    protected BaseActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (BaseActivity) activity;
    }
}
